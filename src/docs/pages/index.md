This is an example

## Button component

The button component can be included within other components like this:

```
\{{> @button }}
```

This template for this component looks like this:

```
{{view @button}}
```

and it therefore expects a set of data to render it that is in the following format:

```
{{context @button}}
```

and the link to access the preview
<a href='/components/preview/button'>Button</a>